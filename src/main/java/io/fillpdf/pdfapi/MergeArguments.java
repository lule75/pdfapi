package io.fillpdf.pdfapi;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

public class MergeArguments {

    private String pdf;
    private Map<String, FieldMapping> fields;
    @JsonProperty
    private boolean flatten = true;

    public boolean shouldFlatten() {
        return flatten;
    }

    public String getPdf() {
        return pdf;
    }

    public Map<String, FieldMapping> getFields() {
        return fields;
    }

}
