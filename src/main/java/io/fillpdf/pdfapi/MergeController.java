package io.fillpdf.pdfapi;

import com.ocdevel.FillpdfService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping("/api/v1/merge")
public class MergeController {

    @RequestMapping(method = RequestMethod.POST)
    ResponseEntity<?> mergePdf(@RequestBody MergeArguments args) {
        // Get the Base64 PDF.
        String pdf = args.getPdf();
        Map<String, FieldMapping> fields = args.getFields();
        boolean shouldFlatten = args.shouldFlatten();

        try {
            FillpdfService fps = new FillpdfService(pdf, "");

            Map<String, String> response = new HashMap<>();

            Set<String> keys = fields.keySet();

            for (String key : keys) {
                Visitor fmp = new FieldMappingProcessor(fps, key);
                // Process the field and fill it into the PDF depending on its type.
                fmp.visit(fields.get(key));
            }

            if (shouldFlatten) {
                response.put("pdf", fps.toByteArray());
            }
            else {
                response.put("pdf", fps.toByteArrayUnflattened());
            }

            return ResponseEntity.ok(response);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

}
