package io.fillpdf.pdfapi;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.lowagie.text.DocumentException;

import java.io.IOException;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type", include = JsonTypeInfo.As.PROPERTY)
@JsonSubTypes({
        @Type(value = TextFieldMapping.class, name = "text"),
        @Type(value = ImageFieldMapping.class, name = "image"),
})
abstract class FieldMapping {

    private String data;

    public abstract void accept(Visitor v) throws IOException, DocumentException;

    public String getData() {
        return this.data;
    }
}
